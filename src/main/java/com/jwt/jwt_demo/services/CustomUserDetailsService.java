package com.jwt.jwt_demo.services;

import com.jwt.jwt_demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    //Getting username from request header
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //Fetching the user in the db
        com.jwt.jwt_demo.entity.User userFound = userRepository.findByUsername(username);
        //Then spring security valides (or not) the credentials of that user with the given authorities
        return new User(userFound.getUsername(), userFound.getPassword(), new ArrayList<>());
    }
}
